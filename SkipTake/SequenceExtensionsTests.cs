using NUnit.Framework;
using SkipTake.Tests;
using System;
using System.Linq;

namespace SkipTakeTests
{
    public class SequenceExtensionsTests
    {
        [TestCase(0)]
        [TestCase(5)]
        [TestCase(10)]
        [TestCase(12)]
        public void TakeFirstNElementsTest(int count)
        {
            var collection = Enumerable.Range(1, 10);

            var takeFirstResult = collection.TakeFirstNElements(count);

            Assert.AreEqual(takeFirstResult.Count(), count > 10 ? 10 : count);

        }

        [TestCase(0)]
        [TestCase(5)]
        [TestCase(10)]
        [TestCase(12)]
        public void SkipLastNElementsTest(int count)
        {
            var collection = Enumerable.Range(1, 10);

            var skipLastResult = collection.SkipLastNElements(count);

            Assert.AreEqual(skipLastResult.Count(), Math.Max(0, 10 - count));
        }
    }
}