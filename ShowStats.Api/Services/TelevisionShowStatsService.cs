using System.Threading.Tasks;
using ShowStats.Api.Models;

namespace ShowStats.Api.Services
{
    public class TelevisionShowStatsService : ITelevisionShowStatsService
    {
        private readonly ITvMazeApi _tvMazeApi;

        public TelevisionShowStatsService(ITvMazeApi tvMazeApi)
        {
            _tvMazeApi = tvMazeApi;
        }

        public async Task<TelevisionShowStats> GetStatsAsync(int showId)
        {
            var show = await _tvMazeApi.GetTelevisionShowAsync(showId);
            var seasons = await _tvMazeApi.GetTelevisionShowSeasonsAsync(showId);

            var repositoryStats = new TelevisionShowStats
            {

                //TODO

            };

            return repositoryStats;
        }
    }
}