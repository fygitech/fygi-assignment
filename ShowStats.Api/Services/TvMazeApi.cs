using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using ShowStats.Api.Models;

namespace ShowStats.Api.Services
{
    public class TvMazeApi : ITvMazeApi
    {
        private readonly HttpClient _httpClient;

        public TvMazeApi(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<TelevisionShow> GetTelevisionShowAsync(int showId)
        {
            var options = new JsonSerializerOptions
            {
                AllowTrailingCommas = false,
                PropertyNameCaseInsensitive = true,
            };

            var response = await _httpClient.GetAsync($"shows/{showId}");
            var content = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<TelevisionShow>(content, options);
        }

        public async Task<IEnumerable<Season>> GetTelevisionShowSeasonsAsync(int showId)
        {
            var options = new JsonSerializerOptions
            {
                AllowTrailingCommas = false,
                PropertyNameCaseInsensitive = true,
            };

            var response = await _httpClient.GetAsync($"shows/{showId}/seasons");
            var content = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<IEnumerable<Season>>(content, options);
        }
    }
}