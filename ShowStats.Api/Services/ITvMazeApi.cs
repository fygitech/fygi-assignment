﻿using ShowStats.Api.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShowStats.Api.Services
{
    public interface ITvMazeApi
    {
        Task<TelevisionShow> GetTelevisionShowAsync(int tvshowId);
        Task<IEnumerable<Season>> GetTelevisionShowSeasonsAsync(int tvshowId);
    }
}
