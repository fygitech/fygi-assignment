﻿using ShowStats.Api.Models;
using System.Threading.Tasks;

namespace ShowStats.Api.Services
{
    public interface ITelevisionShowStatsService
    {
        Task<TelevisionShowStats> GetStatsAsync(int showId);
    }
}
