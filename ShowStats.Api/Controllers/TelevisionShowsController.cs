using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ShowStats.Api.Models;
using ShowStats.Api.Services;

namespace ShowStats.Api.Controllers
{
    [Route("shows")]
    [ApiController]
    public class TelevisionShowsController : ControllerBase
    {
        private readonly ITelevisionShowStatsService _statsService;

        public TelevisionShowsController(ITelevisionShowStatsService statsService)
        {
            _statsService = statsService;
        }

        [HttpGet("{showId}")]
        public async Task<ActionResult<TelevisionShowStats>> GetAsync([FromRoute] int showId)
        {
            var result = await _statsService.GetStatsAsync(showId);
            return Ok(result);
        }
    }
}