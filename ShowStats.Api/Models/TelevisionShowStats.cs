using System;
using System.Collections.Generic;

namespace ShowStats.Api.Models
{
    public class TelevisionShowStats
    {
        public string Title { get; set; } //just a title
        public IDictionary<string, int> WordsStats { get; set; } // count words by number of occurences
        public double AvgSeasonEpisodes { get; set; } // calculate average number of episodes in season
        public double AllEpisodesCount { get; set; } //Sum all episodes from all seasons
        public int GenresCount { get; set; } // count genres
        public DateTime FirstEpisodeReleaseDate { get; set; } // release date of first episode
        public DateTime LastEpisodeReleaseDate { get; set; } // release data of last episode
    }
}