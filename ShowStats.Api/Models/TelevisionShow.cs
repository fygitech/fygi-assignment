using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace ShowStats.Api.Models
{
    public class TelevisionShow
    {
        [JsonPropertyName("id")]
        public long Id { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("genres")]
        public IEnumerable<string> Genres { get; set; }
    }
}