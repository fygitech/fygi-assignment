﻿using System;
using System.Text.Json.Serialization;

namespace ShowStats.Api.Models
{
    public class Season
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("premiereDate")]
        public DateTime PremiereDate { get; set; }

        [JsonPropertyName("endDate")]
        public DateTime EndDate { get; set; }

        [JsonPropertyName("episodeOrder")]
        public int EpisodesCount { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("number")]
        public int Number { get; set; }

        [JsonPropertyName("summary")]
        public string SummaryHtml { get; set; }
    }
}
