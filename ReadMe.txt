ex_1
implement TakeFirstNElements() and SkipLastNElements() methods without using LINQ

Try to make your code efficent


ex_2
1. Add code in StartupExtensions.cs that inject TvMazeApi to IOC container
2. Add code in TelevisionShowStatsService.cs to populate TelevisionShowStats class
3. Handle unsuccessful HTTP requests in TvMazeApi. API End user should see error message: "Can't get data from Maze API".
4. Make all tests pass

tip: tv show with Game of Throne Id=82