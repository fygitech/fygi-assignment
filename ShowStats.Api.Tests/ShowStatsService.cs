﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using ShowStats.Api.Models;
using ShowStats.Api.Services;

namespace ShowStats.Api.Tests
{
    public class StatsServiceTests
    {
        private Mock<ITvMazeApi> _tvMazeApi;
        private ITelevisionShowStatsService _televisionShowStatsService;

        [SetUp]
        public void Setup()
        {
            _tvMazeApi = new Mock<ITvMazeApi>();
            _televisionShowStatsService = new TelevisionShowStatsService(_tvMazeApi.Object);
        }

        [Test]
        public async Task GetStatsAsync_Should_ReturnName()
        {
            // given
            _tvMazeApi
                .Setup(s => s.GetTelevisionShowAsync(It.IsAny<int>()))
                .ReturnsAsync(
                    new TelevisionShow { Id = 1, Name = "some-name", Genres = new List<string>() });

            _tvMazeApi
                .Setup(s => s.GetTelevisionShowSeasonsAsync(It.IsAny<int>()))
                .ReturnsAsync(
                    new List<Season>
                    {
                        new Season {
                            Id = 1,
                            PremiereDate = DateTime.Now.AddDays(-10),
                            EndDate = DateTime.Now,
                            EpisodesCount= 1,
                            Name = "name",
                            SummaryHtml = "<p>summary</p>",
                            Number = 10
                        },
                    });

            // when
            var result = await _televisionShowStatsService.GetStatsAsync(1);

            // then
            Assert.AreEqual(result.Title, "some-name");
        }

        [TestCase(0)]
        [TestCase(1)]
        [TestCase(5)]
        public async Task GetStatsAsync_Should_ReturnGenresCount(int genres)
        {
            // given
            _tvMazeApi
                .Setup(s => s.GetTelevisionShowAsync(It.IsAny<int>()))
                .ReturnsAsync(
                    new TelevisionShow
                    {
                        Genres = Enumerable.Range(0, genres).Select(r => r.ToString()).ToList()
                    });
            _tvMazeApi
                .Setup(s => s.GetTelevisionShowSeasonsAsync(It.IsAny<int>()))
                .ReturnsAsync(
                    new List<Season>
                    {
                        new Season {
                            Id = 1,
                            PremiereDate = DateTime.Now.AddDays(-10),
                            EndDate = DateTime.Now,
                            EpisodesCount= 1,
                            Name = "name",
                            SummaryHtml = "<p>summary</p>",
                            Number = 10
                        },
                    });
            // when
            var result = await _televisionShowStatsService.GetStatsAsync(1);

            // then
            Assert.AreEqual(result.GenresCount, genres);
        }

        [Test]
        public async Task GetStatsAsync_Should_ReturnFirstAndLastEpisodesDates()
        {
            // given
            var now = DateTime.Now;
            _tvMazeApi
                .Setup(s => s.GetTelevisionShowAsync(It.IsAny<int>()))
                .ReturnsAsync(
                    new TelevisionShow
                    {
                        Genres = new List<string>()
                    });
            _tvMazeApi
                .Setup(s => s.GetTelevisionShowSeasonsAsync(It.IsAny<int>()))
                .ReturnsAsync(
                    new List<Season>
                    {
                        new Season {
                            Id = 1,
                            PremiereDate = now.AddDays(-10),
                            EndDate = now.AddDays(-5),
                            EpisodesCount= 1,
                            Name = "name",
                            SummaryHtml = "<p>summary</p>",
                            Number = 10
                        },
                        new Season {
                            Id = 1,
                            PremiereDate = now.AddDays(-5),
                            EndDate = now,
                            EpisodesCount= 1,
                            Name = "name",
                            SummaryHtml = "<p>summary</p>",
                            Number = 10
                        },
                        new Season {
                            Id = 1,
                            PremiereDate = now,
                            EndDate = now.AddDays(5),
                            EpisodesCount= 1,
                            Name = "name",
                            SummaryHtml = "<p>summary</p>",
                            Number = 10
                        },
                    });

            // when
            var result = await _televisionShowStatsService.GetStatsAsync(1);

            // then
            Assert.AreEqual(result.FirstEpisodeReleaseDate, now.AddDays(-10));
            Assert.AreEqual(result.LastEpisodeReleaseDate, now.AddDays(5));
        }

        [Test]
        public async Task GetStatsAsync_Should_ReturnWordsCountWithoutHtmlTags()
        {
            // given
            _tvMazeApi
                .Setup(s => s.GetTelevisionShowAsync(It.IsAny<int>()))
                .ReturnsAsync(
                    new TelevisionShow
                    {
                        Genres = new List<string>()
                    });
            _tvMazeApi
                .Setup(s => s.GetTelevisionShowSeasonsAsync(It.IsAny<int>()))
                .ReturnsAsync(
                    new List<Season>
                    {
                        new Season {
                            Id = 1,
                            PremiereDate = DateTime.Now.AddDays(-10),
                            EndDate = DateTime.Now,
                            EpisodesCount= 1,
                            Name = "name",
                            SummaryHtml = "<p>Lorem ipsum dolor sit amet</p>",
                            Number = 10
                        },
                        new Season {
                            Id = 1,
                            PremiereDate = DateTime.Now.AddDays(-10),
                            EndDate = DateTime.Now,
                            EpisodesCount= 1,
                            Name = "name",
                            SummaryHtml = "<b>Lorem ipsum</b>",
                            Number = 10
                        },
                    });

            // when
            var result = await _televisionShowStatsService.GetStatsAsync(1);

            // then
            CollectionAssert.AreEquivalent(result.WordsStats, new Dictionary<string, int>
            {
                {"Lorem", 2 },
                {"ipsum", 2 },
                {"dolor", 1 },
                {"sit", 1 },
                {"amet", 1 }
            });
        }

        [Test]
        public async Task GetStatsAsync_Should_CalculateEpisodesCountAndAverage()
        {
            // given
            _tvMazeApi
                .Setup(s => s.GetTelevisionShowAsync(It.IsAny<int>()))
                .ReturnsAsync(
                    new TelevisionShow
                    {
                        Genres = new List<string>()
                    });
            _tvMazeApi
                .Setup(s => s.GetTelevisionShowSeasonsAsync(It.IsAny<int>()))
                .ReturnsAsync(
                    new List<Season>
                    {
                        new Season {
                            Id = 1,
                            PremiereDate = DateTime.Now.AddDays(-10),
                            EndDate = DateTime.Now,
                            EpisodesCount= 10,
                            Name = "name",
                            SummaryHtml = "<p>summary</p>",
                            Number = 10
                        },
                        new Season {
                            Id = 1,
                            PremiereDate = DateTime.Now.AddDays(-10),
                            EndDate = DateTime.Now,
                            EpisodesCount= 20,
                            Name = "name",
                            SummaryHtml = "<p>summary</p>",
                            Number = 10
                        },
                    });

            // when
            var result = await _televisionShowStatsService.GetStatsAsync(1);

            // then
            Assert.AreEqual(result.AvgSeasonEpisodes, 15);
            Assert.AreEqual(result.AllEpisodesCount, 30);
        }
    }
}
