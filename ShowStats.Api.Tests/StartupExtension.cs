﻿using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using ShowStats.Api.Services;

namespace ShowStats.Api.Tests
{
    [TestFixture]
    public class StartupExtension
    {
        private WebApplicationFactory<Startup> _factory;

        [OneTimeSetUp]
        public void Setup()
        {
            _factory = new WebApplicationFactory<Startup>();
        }

        [Test]
        public void ShouldResolveGitHubService()
        {
            using var scope = _factory.Services.CreateScope();
            var service = scope.ServiceProvider.GetService<ITvMazeApi>();
            Assert.IsNotNull(service);
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            _factory.Dispose();
        }
    }
}
